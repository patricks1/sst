"""
Simple Schema Transform is an application/library which transforms a set of key-value pairs, a json/python dict,
into a new dictionary based on a set of user defined rules. The power of the transformation engine
relies on a consistent naming convention of the keys because as can be applied based on regex matching.

There are two types of transformations, simple and complex. A simple rule applies a transformation
to either a single field name or value based on the data only for that name-value pair. A complex rule requires
implementing a subtype method which will invoke the specific function for that rule.

# Todos
- Compile regex's in compile step instead of when rule is applied
- Make fully python 2 and 3 compatible
"""

import json
import argparse
import sys
import unicodedata
import re
import functools
import __builtin__
from string import lower, upper
from pprint import pprint 
from datetime import datetime
import traceback
import copy

import logging as log
log.basicConfig(level=log.INFO)

this = sys.modules[__name__]

# Define string constants, maybe make these enums
NAME = "name"
VALUE = "value"
IMPOSSIBLE = "impossible"

TYPE_SIMPLE = "simple"
TYPE_COMPLEX = "complex"
SUBTYPE_SPLIT = "split"

class TransformRule(object):
    """
    TransformRule is a container class for each rules config
    """
    def __init__(self, name, type, regex, function, apply_to=None, find_by = None,
                 params=None, outputs=None, subtype=None, defaults=None):

        self.name = name
        self.function =  function

        self.type = type if type else defaults.get('type') or raise_exc("type required")
        self.regex = regex if regex else defaults.get('regex') or raise_exc("regex required")
        self.apply_to = apply_to if apply_to else defaults.get('apply_to') or raise_exc("apply_to required")
        self.find_by = find_by or defaults.get('find_by') or raise_exc("find_by required")

        self.subtype = subtype
        self.params = params
        self.outputs = outputs


class Config(dict):
    """
    Config is a dict wrapper for the JSON config and rules
    """
    def __init__(self,data):
        super(Config, self).__init__(byteify(data))
        self.rules = []
    
    def set_rule(self, rule, function):
        new_rule = TransformRule(rule.get('name'), rule.get('type'), rule.get('regex'), function,
                                 rule.get('apply_to'), find_by=rule.get('find_by'), params=rule.get('params'),
                                 outputs=rule.get('outputs'), subtype= rule.get("subtype"), defaults=self.get('defaults'))

        self.rules.append(new_rule)


class SST(object):
    """
    SST class handles reading in config and data and checking that functions
    exist in namespace for all transform rules
    """
    def __init__(self, config_path, data_path):
        try:
            self.config = Config(self._load_json(config_path))
            self.data = byteify(self._load_json(data_path))
            self.namespace = this
            self.code = []
            
        except Exception as exc:
            raise Exception("Exception {}".format(exc))

    def compile(self):
        """
        compile validates the config with the namespace to insure all functions can be invoked.
        todo: add parameter validation
        """
        for rule in self.config['rules']:
            func = find_func(rule.get('name'))
            if not func:
                raise Exception("Function {} does not exist in namespace".format(func))

            self.config.set_rule(rule, func)

    def _load_json(self, json_path):

        with open(json_path) as fi:
            data = json.load(fi)

        return data

    def transform(self):
        """
        transform populates value and key funcs that drive transformations breaking it into two stages: simple then complex
        """
        # First apply simple rules
        value_funcs = [r for r in self.config.rules if r.apply_to == VALUE and r.type == TYPE_SIMPLE]
        key_funcs = [r for r in self.config.rules if r.apply_to == NAME and r.type == TYPE_SIMPLE]
        data = apply_transforms(self.data, value_funcs, key_funcs)
        
        # Next apply complex rules
        value_funcs = [r for r in self.config.rules if r.apply_to == VALUE and r.type == TYPE_COMPLEX]
        key_funcs = [r for r in self.config.rules if r.apply_to == NAME and r.type == TYPE_COMPLEX]
        data = apply_transforms(data, value_funcs, key_funcs)
            
        return data


def raise_exc(msg):
    raise Exception(msg)

def find_func(func_name):
    if hasattr(this, func_name):
        return getattr(this, func_name)
    elif hasattr(__builtin__, func_name):
        return getattr(__builtin__, func_name)
    else:
        log.warn("No function found for {}".format(func_name))
        return None

def convert_to_ascii(value):
    value = unicodedata.normalize('NFKC', value).encode('ascii', 'ignore')
    return value

def replace(value, regex, repl):
    value = re.sub(regex, repl, value)
    return value

def apply_split_rule(rule, data):
        """
        A split rule converts one field into many. If the fields do not already exist,
        they are created. Split field rules can be chained together for more complex splitting.
        For example, if you have one field that contains a name and an address. First the
        name and address could be split, and then individual rules could be applied to break
        the address and name into individual components, eg, street, city, state, first,
        middle, last, suffix
        """
        new_data = {}
        for fld_name, fld_val in data.items():
            m = re.search(rule.regex, fld_name)
            if m:
                updated_outputs = dict([(k, v.replace("$1", m.group(1))) for k, v in rule.outputs.items()])
                new_data = rule.function(fld_val, data, **updated_outputs)
        return new_data

def split_date(value, data, month='', day='', year='', format='%m/%d/%Y'):
    """
    Attempts to convert a string into a date and adds fields for month, day, and year parts
    """
    log.debug("Split {} {} {} {}".format(value, month, day, year))
    
    new_data = copy.copy(data)
    try:
        date = datetime.strptime(value, format)

        new_data[month] = date.month
        new_data[year] = date.year
        new_data[day] = date.day
    except:
        log.debug(traceback.print_exc())
        log.debug("Cannot convert {} to datetime".format(value))
        new_data[month] = IMPOSSIBLE
        new_data[year] = IMPOSSIBLE
        new_data[day] = IMPOSSIBLE
    finally:
        return new_data

def byteify(input):
    """
    From https://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-of-unicode-from-json
    """
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def find_complex_wrapper(subtype):

    if subtype == SUBTYPE_SPLIT:
        return apply_split_rule
    else:
        raise Exception("Rule kind: {} not found".format(subtype))

def apply_transforms(data, value_funcs, key_funcs=None, depth=0):
    """
    Applies func lists to every key and value string if they match regex. assumes all are strings, otherwise does nothing to them
    Unicode values are removed.
    """

    if not key_funcs:
        key_funcs = []

    depth +=1 

    def apply_rule(rule, key, val, data = None):
        update_val = val if rule.apply_to == VALUE else key
        filter_val = val if rule.find_by == VALUE else key
        first_param_val = key if rule.apply_to == NAME else val

        if rule.params:
            params = ([first_param_val] + rule.params)
        else:
            params = (first_param_val,)

        m = re.search(rule.regex, filter_val)
        if m:
            if rule.type == TYPE_COMPLEX:
                complex_func = find_complex_wrapper(rule.subtype)
                new_data = complex_func(rule, data)
                return new_data
            else:
                newstr = rule.function(*params)
        else:
            if rule.type == TYPE_COMPLEX:
                return data
            else:
                newstr = update_val

        return newstr

    newdict = copy.copy(data)
    if isinstance(data, dict):
        for key, val in data.iteritems():
            
            newkey = key
            for rule in key_funcs:
                if rule.type == TYPE_COMPLEX:
                    ap = apply_rule(rule, key, val, newdict)
                    newdict.update(ap)
                    log.debug("Update {} at {} with {}\n".format(key, depth, ap))
                else:
                    newkey = apply_rule(rule, key, val)

            if isinstance(val, (str, unicode)):
                if isinstance(val, unicode):
                    val = unicodedata.normalize('NFKC', val).encode('ascii', 'ignore')
                newval = val

                for rule in value_funcs:
                    newval = apply_rule(rule, newkey, newval)

                newdict[newkey] = newval

            if isinstance(val, (list, tuple, dict)):
                newdict[newkey] = apply_transforms(val, value_funcs, key_funcs, depth=depth)

    elif isinstance(data, (list, tuple)):
        newdata = []
        for i in data:
            if isinstance(i, str):
                newval = i
                for rule in value_funcs:
                    newval = apply_rule(rule, '', newval)

                newdata.append(newval)
            elif isinstance(data, list):
                newdata.append(apply_transforms(i, value_funcs, key_funcs, depth=depth))
            else:
                log.debug("{} {} not handled".format(type(i), i))
                newdata.append(i)

        if isinstance(data, tuple):
            newdata = tuple(newdata)
        elif isinstance(data, set):
            newdata = set(newdata)

        return newdata
    else:
        log.debug("Type not handled for {} {}".format(type(data), data))

    return newdict

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("config_path")
    parser.add_argument("data_path")
    args = parser.parse_args()

    sst = SST(args.config_path, args.data_path)
    sst.compile()
    transformed_data = sst.transform()
    pprint(transformed_data)
