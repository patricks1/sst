# README #

SST(Simple Schema Transform) provides a way of transforming a python dict into a new dict by defining transformation rules.


### How do I get set up? ###

There is one sample config, one sample json for data
 - clone, install reqs

 - rules-config contains a sample rules config and record.json sample data
 - Running the script with these args will print the transform
> python sst.py rules-config.json record.json
